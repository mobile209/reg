import 'package:device_preview/device_preview.dart' as dev;
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'class.dart';
import 'class_cell.dart';
// import 'carousel_options.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  static const String _title = 'REG';

  @override
  Widget build(BuildContext context) {
    return dev.DevicePreview(
      tools: const [
        dev.DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        title: _title,
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        theme: ThemeData(
          appBarTheme: AppBarTheme(
            color: Colors.lime.shade700,
          ),
        ),
        home: Main(),
      ),
    );
  }
}

class Main extends StatefulWidget {
  const Main({super.key});

  @override
  State<Main> createState() => _Main();
}

class _Main extends State<Main> {
  static final urlImages = [
    "assets/images/jobFinder.png",
    "assets/images/Gifted.jpg",
    "assets/images/jobFair.jpg",
  ];
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  // ignore: prefer_final_fields
  static List<Widget> _widgetOptions = <Widget>[
    ListView(children: <Widget>[
      Column(children: <Widget>[
        Container(
            width: double.infinity,
            height: 180,
            child: Image.asset(
              "assets/images/main.jpg",
              fit: BoxFit.cover,
            )),
        Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "ยินดีต้อนรับสู่ระบบบริหารการศึกษา",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 22),
                ),
              )
            ],
          ),
        ),
        Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "ข่าวล่าสุด Latest News",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 22),
                ),
              )
            ],
          ),
          decoration: BoxDecoration(color: Colors.grey.shade300),
        ),
        // Container(
        //   child: Table(
        //     children: [
        //       TableRow(
        //         children: [
        //           TableCell(
        //             child: Image.network(
        //               "https://www.buu.ac.th/public/backend/upload/news/thumbnails/news167418825270398700.png",
        //               width: 100,
        //               height: 100,
        //             ),
        //           ),
        //           TableCell(
        //             child: Image.network(
        //               "https://www.buu.ac.th/public/backend/upload/news/thumbnails/news167384431587535000.jpg",
        //               width: 100,
        //               height: 100,
        //             ),
        //           ),
        //         ],
        //         decoration: BoxDecoration(color: Colors.grey.shade300),
        //       ),
        //       TableRow(
        //         children: [
        //           TableCell(
        //             child: Text(
        //               "ขอเชิญบัณฑิตมหาวิทยาลัยบูรพา  ปีการศึกษา 2564 ตอบแบบสำรวจภาวะการมีงานทำของบัณฑิต",
        //               textAlign: TextAlign.center,
        //             ),
        //           ),
        //           TableCell(
        //             child: Text(
        //               "สำนักหอสมุด จัดงานบุ๊คแฟร์ Burapha BookFair 2023",
        //               textAlign: TextAlign.center,
        //             ),
        //           ),
        //         ],
        //         decoration: BoxDecoration(color: Colors.grey.shade300),
        //       ),
        //       TableRow(
        //         children: [
        //           TableCell(
        //             child: Image.network(
        //               "https://www.buu.ac.th/public/backend/upload/news/thumbnails/news167333748973232200.jpg",
        //               width: 100,
        //               height: 100,
        //             ),
        //           ),
        //           TableCell(
        //             child: Image.network(
        //               "https://mpics.mgronline.com/pics/Images/565000010632101.JPEG",
        //               width: 100,
        //               height: 100,
        //             ),
        //           ),
        //         ],
        //         decoration: BoxDecoration(color: Colors.grey.shade300),
        //       ),
        //       TableRow(
        //         children: [
        //           TableCell(
        //             child: Text(
        //               "ขอเชิญบัณฑิตมหาวิทยาลัยบูรพา  ปีการศึกษา 2564 ตอบแบบสำรวจภาวะการมีงานทำของบัณฑิต",
        //               textAlign: TextAlign.center,
        //             ),
        //           ),
        //           TableCell(
        //             child: Text(
        //               "ภาคต่อ เกมทำไร่โดราเอมอน วางจำหน่ายวันนี้ มีภาษาไทย",
        //               textAlign: TextAlign.center,
        //             ),
        //           ),
        //         ],
        //         decoration: BoxDecoration(color: Colors.grey.shade300),
        //       ),
        //     ],
        //   ),
        // ),
        Container(
            decoration: BoxDecoration(color: Colors.grey.shade300),
            margin: EdgeInsets.symmetric(horizontal: 5.0),
            child: CarouselSlider.builder(
              options: CarouselOptions(
                height: 200,
                viewportFraction: 1,
                enlargeCenterPage: true,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 2),
              ),
              itemCount: urlImages.length,
              itemBuilder: (context, index, realIndex) {
                final urlImage = urlImages[index];

                return buildImage(urlImage, index);
              },
            ))
      ]),
    ]),
    ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              child: Padding(
                padding: EdgeInsets.all(16),
                child: Container(child: Image.asset("assets/images/class.png")),
              ),
            ),
            Container(
                child: Padding(
              padding: EdgeInsets.all(16),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 1.0, color: Colors.black),
                ),
                child: Table(
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  children: <TableRow>[
                    TableRow(
                      decoration: BoxDecoration(
                        color: Colors.lime[200],
                      ),
                      children: const <Widget>[
                        ClassCell(title: 'รหัสวิชา', isHeader: true),
                        ClassCell(title: 'ชื่อรายวิชา', isHeader: true),
                        ClassCell(title: 'กลุ่ม', isHeader: true),
                        ClassCell(title: 'สอบกลางภาค', isHeader: true),
                        ClassCell(title: 'สอบปลายภาค', isHeader: true),
                      ],
                    ),
                    ...Class.getClass().map((classE) {
                      return TableRow(children: [
                        ClassCell(title: classE.code),
                        ClassCell(title: classE.name),
                        ClassCell(title: '${classE.group}'),
                        ClassCell(title: classE.mid),
                        ClassCell(title: classE.last),
                      ]);
                    }),
                  ],
                ), // Your table contents go here
              ),
            ))
          ],
        )
      ],
    ),
    ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(child: Image.asset("assets/images/grade.png"))
          ],
        )
      ],
    ),
    SettingsList(
      sections: [
        SettingsSection(
          tiles: <SettingsTile>[
            SettingsTile.navigation(
              title: Text('ภาษา'),
              value: Text('English'),
            ),
            SettingsTile.switchTile(
              onToggle: (value) {},
              initialValue: true,
              title: Text('เปิดการแจ้งเตือน'),
            ),
          ],
        ),
      ],
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: const Text(
            'BUU REG',
          ),
        ),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            backgroundColor: Colors.blueGrey,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Schedule',
            backgroundColor: Colors.blueGrey,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: 'Grade',
            backgroundColor: Colors.blueGrey,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
            backgroundColor: Colors.blueGrey,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.lime[800],
        onTap: _onItemTapped,
      ),
    );
  }
}

Widget buildImage(String urlImage, int index) => Container(
      margin: EdgeInsets.symmetric(horizontal: 7),
      color: Colors.white,
      child: Image.asset(
        urlImage,
        fit: BoxFit.cover,
      ),
    );

//   @override
//   Widget build(BuildContext context) {
//     int _selectedIndex = 0;
//     return MaterialApp(
//         routes: {
//           '/second': (context) => Grade(),
//         },
//       debugShowCheckedModeBanner: false,
//       title: 'BUU REG',
//       theme: ThemeData(
//         appBarTheme: AppBarTheme(
//           color: Colors.lime.shade700,
//         ),
//       ),
//       home: Builder(
//         builder: (context){
//           return Scaffold(
//             backgroundColor: Colors.white,
//             appBar: AppBar(
//               title: const Text("REG BUU"),
//             ),
//
//             body: ListView(
//               children: <Widget>[
//                 Column(
//                   children: <Widget>[
//                     Container(
//                         width: double.infinity,
//                         height: 180,
//                         child:Image.network(
//                           "http://scgi.gistda.or.th/wp-content/uploads/2018/02/maxresdefault-e1518406033109.jpg",
//                           fit:BoxFit.cover ,
//
//                         )
//                     ),
//                     Container(
//                       height: 60,
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.start ,
//
//                         children: <Widget>[
//                           Padding(
//                             padding:EdgeInsets.all(8.0),
//                             child: Text(
//                               "ยินดีต้อนรับสู่ระบบบริหารการศึกษา",
//                               textAlign:TextAlign.center ,
//                               style: TextStyle(fontSize: 22),
//                             ),
//                           )
//                         ],
//                       ),
//                     ),
//                     Container(
//                       height: 60,
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.start ,
//
//                         children: <Widget>[
//                           Padding(
//                             padding:EdgeInsets.all(8.0),
//                             child: Text(
//                               "ข่าวล่าสุด Latest News",
//                               textAlign:TextAlign.center ,
//                               style: TextStyle(fontSize: 22),
//                             ),
//                           )
//                         ],
//                       ),
//                       decoration: BoxDecoration(color: Colors.grey.shade300),
//                     ),
//                     Container(
//                         child:Table(
//                           children: [
//                             TableRow(
//                               children: [
//                                 TableCell(
//                                   child: Image.network(
//                                     "https://www.buu.ac.th/public/backend/upload/news/thumbnails/news167418825270398700.png",
//                                     width: 100,
//                                     height: 100,
//                                   ),
//                                 ),
//
//                                 TableCell(
//                                   child: Image.network(
//                                     "https://www.buu.ac.th/public/backend/upload/news/thumbnails/news167384431587535000.jpg",
//                                     width: 100,
//                                     height: 100,
//                                   ),
//                                 ),
//                               ],
//                               decoration: BoxDecoration(color: Colors.grey.shade300),
//                             ),
//                             TableRow(
//                               children: [
//                                 TableCell(
//                                   child: Text("ขอเชิญบัณฑิตมหาวิทยาลัยบูรพา  ปีการศึกษา 2564 ตอบแบบสำรวจภาวะการมีงานทำของบัณฑิต",
//                                     textAlign: TextAlign.center,
//                                   ),
//                                 ),
//                                 TableCell(
//                                   child: Text("สำนักหอสมุด จัดงานบุ๊คแฟร์ Burapha BookFair 2023",
//                                     textAlign: TextAlign.center,
//                                   ),
//                                 ),
//                               ],
//                               decoration: BoxDecoration(color: Colors.grey.shade300),
//                             ),
//                             TableRow(
//                               children: [
//                                 TableCell(
//                                   child: Image.network(
//                                     "https://www.buu.ac.th/public/backend/upload/news/thumbnails/news167333748973232200.jpg",
//                                     width: 100,
//                                     height: 100,
//                                   ),
//                                 ),
//
//                                 TableCell(
//                                   child: Image.network(
//                                     "https://mpics.mgronline.com/pics/Images/565000010632101.JPEG",
//                                     width: 100,
//                                     height: 100,
//                                   ),
//                                 ),
//                               ],
//                               decoration: BoxDecoration(color: Colors.grey.shade300),
//                             ),
//                             TableRow(
//                               children: [
//                                 TableCell(
//                                   child: Text("ขอเชิญบัณฑิตมหาวิทยาลัยบูรพา  ปีการศึกษา 2564 ตอบแบบสำรวจภาวะการมีงานทำของบัณฑิต",
//                                     textAlign: TextAlign.center,
//                                   ),
//                                 ),
//                                 TableCell(
//                                   child: Text("ภาคต่อ เกมทำไร่โดราเอมอน วางจำหน่ายวันนี้ มีภาษาไทย",
//                                     textAlign: TextAlign.center,
//                                   ),
//                                 ),
//                               ],
//                               decoration: BoxDecoration(color: Colors.grey.shade300),
//                             ),
//                           ],
//                         )
//                     ),
//
//                   ],
//                 )
//               ],
//
//             ),
//             bottomNavigationBar: BottomNavigationBar(
//               items: const <BottomNavigationBarItem>[
//                 BottomNavigationBarItem(
//                   icon: Icon(Icons.home),
//                   label: 'Home',
//                 ),
//                 BottomNavigationBarItem(
//                   icon: Icon(Icons.business),
//                   label: 'Business',
//                 ),
//                 BottomNavigationBarItem(
//                   icon: Icon(Icons.school),
//                   label: 'School',
//                 ),
//               ],
//               currentIndex: _selectedIndex,
//               selectedItemColor: Colors.lime[800],
//               onTap: _onItemTapped,
//             ),
//           );
//         }
//       )
//     );
//   }
// }
