class Class {
  final String code;
  final String name;
  final int group;
  final String mid;
  final String last;

  Class(this.code, this.name, this.group, this.mid, this.last);

  static List<Class> getClass() {
    return [
      Class(
          '88624359-59', 'Web Programming', 1, '16 ม.ค. 2566', '27 มี.ค. 2566'),
      Class('88624459-59', 'Object-Oriented Analysis and Design', 1,
          '17 ม.ค. 2566', '28 มี.ค. 2566'),
      Class('88624559-59', 'Software Testing', 1, '16 ม.ค. 2566',
          '27 มี.ค. 2566'),
      Class('88634259-59', 'Multimedia Programming for Multiplatforms', 1,
          '18 ม.ค. 2566', '29 มี.ค. 2566'),
      Class('88634459-59', 'Mobile Application Development I', 1,
          '20 ม.ค. 2566', '29 มี.ค. 2566'),
      Class('88646259-59', 'Introduction to Natural Language Processing', 1,
          '18 ม.ค. 2566', '31 มี.ค. 2566'),
    ];
  }
}
